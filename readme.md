## Installation instructions
Add the `autoexec.cfg` file and the `includes` directory to the following path: `<steam_install_path>\userdata\2649124\730\local\cfg`  

for example:  
`D:\Program Files (x86)\Steam\userdata\2649124\730\local\cfg`  

Then open the `autoexec.cfg` in your favorite editor and edit the includes you wish to be executed.  
When you start the game you can check your console if the config is successfully loaded.

-----

## Active Launch Options
`-console -novid -nojoy -threads 8 -tickrate 128 -freq 120 -full -language english`  
  
-----

## All Launch Options
* `-console`  
  : This launch option will open the console automatically when the game has launched.  
  
* `-novid`  
  : This will remove the Valve intro when launching the game.  
  
* `-tickrate 128`  
  : This will force your game to run at a tickrate of 128 where possible.


* `-refresh <rate> / -refreshrate <rate> / -freq <rate>`  
  : This will force the game to run at the given refresh rate. Set this to whatever your monitor can handle.  
  : **WARNING: CAN DAMAGE MONITOR IF SET HIGHER THEN MONITOR CAN HANDLE.**


* `-high`  
  : This will start the game in high-priority mode.


* `-threads <number of cores/threads>`  
  : So far, I was not able to find a definitive information about the maximum number of threads that CS:GO uses and if this launchoption makes any sense. If you have a CPU with 4 or more cores, you can try to set -threads to the number of cores (or number of threads if you own a CPU with 2 threads/core) you have, but right now I can not guarantee that your performance will improve. Test it, if you don’t notice any difference or your performance is actually worse, remove the launchoption again.


* `-full / -fullscreen`  
  : This launch option forces the game to run in fullscreen mode. The game will ignore this launch option, if -windowed / -window / -sw / -startwindowed option is also set.


* `-windowed / -window / -sw / -startwindowed`
  : This will force the game to run in windowed mode. Should not be set without -w and -h also being specified. The game will ignore this launch option, if -full / -fullscreen option is also set.

* `-w <width> / -width <width>`  
  : N/A
  
  
* `-h <height> / -height <height>`  
  : This forces the game to start with the resolution you specified, e.g. -w 1920 -h 1080.


* `-noborder`  
  : Using this launch option will remove the border that Windows puts around the window when the game is run in windowed mode.


* `-x <position> –horizontal`  
  : N/A
  
  
* `-y <position> –vertical`  
  : When the game is run with no border, you can’t move the window around and it is stuck to the center of your screen. You can define the position of the window with these 2 launch options. <position> is the space in